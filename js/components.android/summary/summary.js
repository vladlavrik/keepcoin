'use strict';
import React, {Component, PropTypes} from 'react';
import {
    StyleSheet,
    ToolbarAndroid,
    Text,
    View
} from 'react-native';

import ActionButtonTransaction from '../common/action-button-transaction';

const styles = StyleSheet.create({
    toolbar: {
        height: 56,
        backgroundColor: '#009688'
    }
});

export default class Summary extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }
    render() {


        return (
            <View style={ {backgroundColor: "#ccc", flex: 1} }>
                <ToolbarAndroid
                    titleColor="#fff"
                    title="Сводка"
                    style={styles.toolbar}
                    navIcon={{uri: 'ic_menu_white_24dp'}}
                    onIconClicked={ this.context.openDrawer }
                />
                <Text>Summary</Text>
                <ActionButtonTransaction/>
            </View>
        )
    }
}


Summary.contextTypes = {
    openDrawer: PropTypes.func.isRequired,
    navigateTo: PropTypes.func.isRequired
};