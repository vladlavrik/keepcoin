'use strict';
import React, {Component, PropTypes} from 'react';
import {
    AppRegistry,
    StyleSheet,
    StatusBar,
    DrawerLayoutAndroid,
    Navigator
} from 'react-native';
import DB from '../lib/db';
import BackAndroid from 'BackAndroid';
import Drawer from './common/drawer';
import Summary from './summary/summary';
import Transactions from './transactions/transactions';
import Transaction from './transaction/transaction';
import Statistics from './statistics/statistics';
import Cards from './cards/cards';
import Pay from './pay/pay';
import Profit from './profit/profit';
import Settings from './settings/settings';
import Help from './help/help';

const styles = StyleSheet.create({
    navigator: {
        flex: 1,
        backgroundColor: '#eee',
    },
});

// todo status bar transparent, re render on change orientation

export default class KeepCoin extends Component {
    constructor(props) {
        super(props);

        // initialize Database
        this.db = new DB({name: 'master.db', createFromLocation: 1});

        this.state = {};
    }

    //noinspection JSUnusedGlobalSymbols
    getChildContext() {
        return {
            db: this.db,
            openDrawer: ::this.openDrawer,
            navigateBack: ::this.navigateBack,
            navigateTo: ::this.navigateTo
        };
    }

    render() {
        return (
            <DrawerLayoutAndroid
                ref="drawer"
                drawerWidth={300}
                drawerPosition={DrawerLayoutAndroid.positions.Left}
                renderNavigationView={() => <Drawer/>}>

                <StatusBar backgroundColor="#00796B"/>

                <Navigator
                    style={ styles.navigator }
                    ref="navigator"
                    renderScene={::this.renderScene}
                    configureScene={::this.configureScene}
                    initialRoute={{
                        name: 'transactions'
                    }}
                />

            </DrawerLayoutAndroid>
        )
    }

    componentDidMount() {
        BackAndroid.addEventListener('hardwareBackPress', ::this.onHardwareBack);
    }

    renderScene(route, navigator) {
        switch (route.name) {
            case 'transactions':
                return <Transactions/>;
            case 'transaction':
                return <Transaction transaction={route.transaction}/>;
            case 'summary':
                return <Summary/>;
            case 'statistics':
                return <Statistics/>;
            case 'settings':
                return <Settings/>;
            case 'cards':
                return <Cards/>;
            case 'pay':
                return <Pay/>;
            case 'profit':
                return <Profit/>;
            case 'help':
                return <Help/>;
        }
    }

    //noinspection JSMethodCanBeStatic
    configureScene(route, routeStack) {
        switch (route.name) {
            case 'transaction':
                return Navigator.SceneConfigs.PushFromRight;
            case 'pay':
            case 'profit':
                return Navigator.SceneConfigs.FloatFromBottomAndroid;
            default:
                return Navigator.SceneConfigs.FadeAndroid;
        }
    }

    openDrawer() {
        this.refs.drawer.openDrawer();
    }

    navigateBack() {
        this.refs.navigator.pop();
    }

    navigateTo(route) {
        this.refs.drawer.closeDrawer(); //todo check need;
        this.refs.navigator.push(route);
    }

    //todo on hard key back tap, if open drawer ? hide drawer : navigateBack
    onHardwareBack() {
        let routesList = this.refs.navigator.getCurrentRoutes();
        if (routesList.length > 1) {
            this.navigateBack();
            return true;
        }
    }
}

KeepCoin.childContextTypes = {
    db: PropTypes.instanceOf(DB).isRequired,
    openDrawer: PropTypes.func,
    navigateBack: PropTypes.func,
    navigateTo: PropTypes.func
};

AppRegistry.registerComponent('KeepCoin', () => KeepCoin);
