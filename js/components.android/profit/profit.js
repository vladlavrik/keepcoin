'use strict';
import React, {Component, PropTypes} from 'react';
import {
    StyleSheet,
    ToolbarAndroid,
    Text,
    View
} from 'react-native';

const styles = StyleSheet.create({
    toolbar: {
        height: 56,
        backgroundColor: "#009688"
    }
});
// TODO if BACK - lock drawer ang go back on swipe
export default class Profit extends Component {
    render() {
        return (
            <View style={ {backgroundColor: "#009688", flex: 1 } }>

                <ToolbarAndroid
                    titleColor="#fff"
                    title="Добавить прибыль"
                    style={styles.toolbar}
                    navIcon={{uri: 'ic_arrow_back_white_24dp'}}
                    onIconClicked={ this.context.navigateBack }
                    actions={[{
                        title: 'Save',
                        icon: {uri: 'ic_done_white_24dp'},
                        show: 'always'
                    }]}
                    onActionSelected={ ::this.save }
                />

                <Text>Profit</Text>
            </View>
        )
    }

    save() {
        console.log('SAVE');
        this.context.navigateBack()
    }
}

Profit.contextTypes = {
    navigateBack: PropTypes.func.isRequired
};