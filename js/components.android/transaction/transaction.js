'use strict';
import React, {Component, PropTypes} from 'react';
import {
    StyleSheet,
    Dimensions,
    ScrollView,
    ToolbarAndroid,
    Image,
    Text,
    View
} from 'react-native';
import DB from '../../lib/db';

const styles = StyleSheet.create({
    toolbar: {
        height: 56,
        backgroundColor: "#009688"
    }
});

const getTransactionSQl = `
    select
        transactions._id as id,
        transactions.total,
        cards.name as card,
        categories.name as category,
        (CASE categories.type WHEN 0 THEN 'pay' WHEN 1 THEN 'profit' END)  as categoryType,
        transactions.timestamp,
        transactions.comment,
        transactions.latitude,
        transactions.longitude,
        transactions.address,
        group_concat(transactionsPhotos.path, ':') as photos
    from transactions
    left outer join cards on transactions.cardId = cards._id
    left outer join categories on transactions.categoryId = categories._id
    left outer join transactionsPhotos on transactionsPhotos.transactionId = transactions._id
    where id=?
    group by id
`;

export default class Transaction extends Component {
    constructor(props) {
        super(props);

        this.state = {
            loading: true,
            longitude: null,
            latitude: null,
            address: null,
            photos: [],
            ...props.transaction
        }
    }
    componentWillMount() {
        //noinspection JSIgnoredPromiseFromCall
        this.loadData();
    }

    async loadData() {
        let id = this.props.transaction.id;
        let {rows} = await this.context.db.query(getTransactionSQl, [id]);
        let data = rows.item(0);

        this.setState({
            total: data.total,
            comment: data.comment,
            category: data.category,
            categoryType: data.categoryType,
            card: data.card,
            timestamp: data.timestamp,
            longitude: data.longitude,
            latitude: data.latitude,
            address: data.address,
            photos: data.photos
                ? data.photos.split(':').map(path => ({uri: 'file://' + path}))
                : [],
        }, ::this.loadPhotoSizes);
    }

    async loadPhotoSizes() {
        let sizes = await Promise.all(
            this.state.photos.map(({uri}) =>
                new Promise((resolve, reject) => {
                    Image.getSize(
                        uri,
                        (width, height) => resolve({width, height}),
                        reject
                    )
                })
            )
        );

        this.setState({
            photos: this.state.photos.map(({uri}, index) => {
                let {width, height} = sizes[index];
                return {uri, width, height};
            })
        });
    }

    render() {
        let {width: screenWidth} = Dimensions.get('window');

        return (
            <View style={ {backgroundColor: "#ddd", flex: 1 } }>

                <ToolbarAndroid
                    titleColor="#fff"
                    title="Трансакция"
                    style={styles.toolbar}
                    navIcon={{uri: 'ic_arrow_back_white_24dp'}}
                    onIconClicked={ this.context.navigateBack }
                />


                <ScrollView
                    style={styles.root}>

                    <View>
                        <Text>ID {this.state.id}</Text>
                        <Text>total {this.state.total}</Text>
                        <Text>comment {this.state.comment}</Text>
                        <Text>category {this.state.category}</Text>
                        <Text>categoryType {this.state.categoryType}</Text>
                        <Text>card {this.state.card}</Text>
                        <Text>timestamp {this.state.timestamp}</Text>
                        <Text>longitude {this.state.longitude}</Text>
                        <Text>latitude {this.state.latitude}</Text>
                        <Text>address {this.state.address}</Text>
                        {this.state.photos.map((photo, key) => {
                            let {width, height} = photo;
                            if (!width || !height) return null;

                            let ratio = (screenWidth - 16 * 2) / width;
                            width = width * ratio;
                            height = height * ratio;
                            //todo check case if width < quality

                            return <Image key={key} source={photo} style={{width, height, marginHorizontal: 16}}/>
                        }) }
                    </View>

                </ScrollView>

            </View>
        )
    }
}

Transaction.contextTypes = {
    db: PropTypes.instanceOf(DB).isRequired,
    navigateBack: PropTypes.func.isRequired
};

Transaction.propsTypes= {
    transaction: PropTypes.shape({
        id: PropTypes.number.isRequired,
        total: PropTypes.number.isRequired,
        comment: PropTypes.string.isRequired,
        category: PropTypes.string.isRequired,
        categoryType: PropTypes.number.isRequired,
        card: PropTypes.string.isRequired,
        timestamp: PropTypes.number.isRequired,
        hasLocation: PropTypes.bool.isRequired,
        hasPhotosCount: PropTypes.bool.isRequired
    }).isRequired
};


