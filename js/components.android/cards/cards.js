'use strict';
import React, {Component, PropTypes} from 'react';
import {
    StyleSheet,
    ToolbarAndroid,
    Text,
    View
} from 'react-native';

const styles = StyleSheet.create({
    toolbar: {
        height: 56,
        backgroundColor: "#009688"
    }
});

export default class Cards extends Component {
    render() {
        return (
            <View>
                <ToolbarAndroid
                    titleColor="#fff"
                    title="Способы оплаты"
                    style={styles.toolbar}
                    navIcon={{uri: 'ic_menu_white_24dp'}}
                    onIconClicked={ this.context.openDrawer }
                />
                <Text style={{ paddingHorizontal: 16, paddingTop: 8}}>Cards</Text>
            </View>
        )
    }
}


Cards.contextTypes = {
    openDrawer: PropTypes.func.isRequired
};