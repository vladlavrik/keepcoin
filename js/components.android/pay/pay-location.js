'use strict';
import React, {Component, PropTypes} from 'react';
import {
    StyleSheet,
    TouchableWithoutFeedback,
    Image,
    Text,
    View
} from 'react-native';

const styles = StyleSheet.create({
    root: {
        padding: 8,
        height: 76
    },
    buttonRemove: {
        position: 'absolute',
        top: 0,
        right: 0
    },
    wrapper: {
        width: 156,
        height: 70,
        backgroundColor: '#E0F2F188',
        padding: 8,
    },
    buttonRemoveImage: {
        width: 24,
        height: 24
    },
    title: {
        fontSize: 12,
        color: '#E0F2F1aa',
    },
    text: {
        fontSize: 12,
        color: '#E0F2F1ee',
        height: 40,
        textAlignVertical: 'center'
    }
});

export default class PayLocation extends Component {
    render() {
        let {address, latitude, longitude, onRemove} = this.props;
        return (
            <View style={styles.root}>
                <View style={styles.wrapper}>
                    <Text style={styles.title}>Местоположение:</Text>

                    <Text style={styles.text} numberOfLines={2}>
                        {address ? address : 'x: ' + latitude.toFixed(4) + '\ny: ' + longitude.toFixed(4)}
                    </Text>
                </View>
                <TouchableWithoutFeedback onPress={onRemove}>
                    <View style={styles.buttonRemove}>
                        <Image
                            source={{uri:'ic_cancel_black_24px'}}
                            style={styles.buttonRemoveImage}/>
                    </View>
                </TouchableWithoutFeedback>
            </View>
        );
    }
}

PayLocation.propTypes = {
    latitude: PropTypes.number.isRequired,
    longitude: PropTypes.number.isRequired,
    address: PropTypes.string,
    onRemove: PropTypes.func.isRequired
};