'use strict';
import React, {Component, PropTypes} from 'react';
import {
    StyleSheet,
    TextInput,
    Picker,
    View
} from 'react-native';
import DB from '../../lib/db';

const getCardsListSQl = `
    select _id as id, name
    from cards`;

const styles = StyleSheet.create({
    root: {
        marginHorizontal: 10,
        marginTop: 10
    },
    textInput: {
        color:'#F5F5F5',
        paddingRight:88,
        height:54
    },
    picker: {
        position: 'absolute',
        width: 140,
        height: 40,
        top: 4,
        right: 10,
        color: '#F5F5F5'
    }
});

const totalRegexp = /^([0-9]+)(\.[0-9]+)?$/;


export default class PayTotal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            totalValue: '',
            totalError: false,
            cardsList: [],
            cardIdValue: null, //TODO get default from settings
        };
    }

    componentWillMount(){
        this.context.db.query(getCardsListSQl).then(::this.cardsListLoaded, ::this.cardsListLoadFail);
    }

    render() {
        let {
            totalValue,
            cardsList,
            cardIdValue,
            totalError
        } = this.state;

        return (
            <View style={styles.root}>
                <TextInput
                    ref="total"
                    placeholder="Сумма"
                    keyboardType="numeric"
                    maxLength={10}
                    placeholderTextColor="#F5F5F588"
                    selectionColor="#d32f2f"
                    returnKeyLabel="next"
                    underlineColorAndroid={totalError ? '#e53935' : '#80CBC4'}
                    style={styles.textInput}
                    value={totalValue}
                    onChangeText={::this.changeTotal}
                />
                {cardsList.length > 0 && (
                    <Picker
                        ref="card"
                        mode="dropdown"
                        style={styles.picker}
                        selectedValue={cardIdValue}
                        onValueChange={::this.changeCard}>
                        { cardsList.map(({id, name}) => (
                            <Picker.Item key={id} label={name} value={id} />
                        ))}
                    </Picker>
                )}
            </View>
        );
    }

    cardsListLoaded({rows}) {
        let cardsList = rows.raw();
        this.setState({
            cardsList,
            cardIdValue: cardsList[0].id //todo set from config
        });
    }

    cardsListLoadFail(error) {
        // todo show error
    }

    changeTotal(totalValue) {
        this.setState({totalValue, totalError: false});
    }

    changeCard(cardIdValue) {
        this.setState({cardIdValue});
    }

    getValue() {
        let total = this.state.totalValue;
        let card = this.state.cardIdValue;

        if (totalRegexp.test(total)) {
            total = parseFloat(total);
        } else {
            this.setState({totalError: true});
            total = null;
        }

        return {total, card};
    }
}

PayTotal.contextTypes = {
    db: PropTypes.instanceOf(DB).isRequired
};