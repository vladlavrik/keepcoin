'use strict';
import React, {Component} from 'react';
import {
    StyleSheet,
    ActivityIndicator,
    ToastAndroid,
    TouchableNativeFeedback,
    TextInput,
    Image,
    View
} from 'react-native';
import RNFS from 'react-native-fs';
import config from '../../config.json';
import {getCoordinates, getAddress} from '../../lib/location-helper';
import PayLocation from './pay-location';
import PayPhoto from './pay-photo';
import ImagePicker from '../common/image-picker/image-picker';

const TouchableBg = TouchableNativeFeedback.SelectableBackgroundBorderless();

const styles = StyleSheet.create({
    root: {
        marginTop: 20
    },
    textInput: {
        color: '#F5F5F5',
        paddingRight: 88,
        marginHorizontal: 12,
    },
    button: {
        position:'absolute',
        top: 14,
        opacity: 0.75
    },
    buttonPhoto: {
        right: 24
    },
    buttonLocation: {
        right: 64
    },
    buttonImage: {
        width: 24,
        height: 24
    },
    buttonDisabled: {
        opacity: 0.3
    },
    attaches: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        paddingHorizontal: 8,
    }
});


export default class PayComment extends Component {
    constructor(props) {
        super(props);
        this.state = {
            commentText: '',
            commentHeight: 0,
            locationLoading: false,
            locationCoordinates: null,
            locationAddress: null,
            photoPickerShown: false,
            photoList: []
        };
    }

    render() {
        let {
            commentText,
            commentHeight,
            locationLoading,
            locationCoordinates,
            locationAddress,
            photoPickerShown,
            photoList
        } = this.state;

        let disabledAddLocation = !!locationCoordinates;
        let disabledAddPhoto = photoList.length >= config.photoTransactionMaxCount;

        return (
            <View style={styles.root}>
                <TextInput
                    ref="text"
                    placeholder="Описание покупки"
                    placeholderTextColor="#F5F5F588"
                    multiline
                    selectionColor="#d32f2f"
                    returnKeyLabel="next"
                    underlineColorAndroid="#80CBC4"
                    style={[styles.textInput, {
                        height: Math.max(54, commentHeight)
                    }]}
                    value={commentText}
                    onChangeText={::this.changeCommentText}
                    onContentSizeChange={::this.changeCommentSize}
                />

                <TouchableNativeFeedback
                    background={TouchableBg}
                    onPress={locationLoading || disabledAddLocation ? null : ::this.addLocation}>
                    <View style={[
                        styles.button,
                        styles.buttonLocation,
                        disabledAddLocation && styles.buttonDisabled
                        ]}>
                        {locationLoading
                            ? <ActivityIndicator
                                size="small"
                                color="#ffffffbb"
                                style={styles.buttonImage}/>
                            : <Image
                                source={{uri:'ic_place_white_24dp'}}
                                style={styles.buttonImage}/>
                        }
                    </View>
                </TouchableNativeFeedback>
                <TouchableNativeFeedback
                    background={TouchableBg}
                    onPress={photoPickerShown || disabledAddPhoto ? null : ::this.photoPickerShow}>
                    <View style={[
                        styles.button,
                        styles.buttonPhoto,
                        disabledAddPhoto && styles.buttonDisabled
                    ]}>
                        <Image
                            source={{uri:'ic_camera_white_24dp'}}
                            style={styles.buttonImage}/>
                    </View>
                </TouchableNativeFeedback>

                <View style={styles.attaches}>
                    {locationCoordinates && (
                        <PayLocation
                            latitude={locationCoordinates[0]}
                            longitude={locationCoordinates[1]}
                            address={locationAddress}
                            onRemove={::this.removeLocation}
                        />
                    )}
                    {!!photoList.length && photoList.map((path, key) => (
                        <PayPhoto
                            key={key}
                            path={path}
                            onRemove={this.removePhoto.bind(this, path)}/>
                    ))}
                </View>

                {photoPickerShown && (
                    <ImagePicker
                        visible
                        quality={config.photoTransactionQuality}
                        photoTransactionQuality
                        onStartSave={() => 'onStartSave'}
                        onSaved={::this.addSavedImage}
                        onCancel={::this.photoPickerClose}
                        />
                )}
            </View>
        );
    }

    changeCommentText(commentText) {
        this.setState({commentText});
    }

    changeCommentSize(event) {
        this.setState({commentHeight: event.nativeEvent.contentSize.height});
    }

    async addLocation() {
        //TODO add feature:
        // - manual set location
        // - select form list addresses from google maps api
        this.setState({locationLoading: true});

        let coordinates;
        try {
            let {latitude, longitude} = await getCoordinates();
            coordinates = [latitude, longitude];
            this.setState({locationCoordinates: coordinates});
        } catch (err) {
            ToastAndroid.show('Ошибка получения геоданных', ToastAndroid.SHORT);
            this.setState({locationLoading: false});
            return;
            //TODO report error
        }

        try {
            let address = await getAddress(...coordinates);
            this.setState({locationLoading: false, locationAddress: address});
        } catch (err) {
            this.setState({locationLoading: false});
            //TODO report error
        }
    }

    photoPickerShow() {
        this.setState({ photoPickerShown: true });
    }

    photoPickerClose() {
        this.setState({ photoPickerShown: false });
    }

    addSavedImage(path) {
        this.setState({
            photoPickerShown: false,
            photoList: [...this.state.photoList, path]
        });
    }

    removeLocation() {
        this.setState({
            locationAddress: null,
            locationCoordinates: null
        });
    }

    async removePhoto(path) {
        this.setState({
            photoList: this.state.photoList.filter(_path => _path !== path)
        });
        try {
            await RNFS.unlink(path);
        } catch (err) {}
    }

    getValue() {
        return {
            text: this.state.commentText.replace(/^\s+|\s+$/g, ''),
            photos: this.state.photoList,
            coordinates: this.state.locationCoordinates,
            address: this.state.locationAddress,
        };
    }
}