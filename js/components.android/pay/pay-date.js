'use strict';
import React, {Component} from 'react';
import {
    StyleSheet,
    TouchableNativeFeedback,
    DatePickerAndroid,
    ToastAndroid,
    Image,
    Text,
    View
} from 'react-native';
import leftPad from '../../lib/utils/left-pad';

const TouchableBg = TouchableNativeFeedback.SelectableBackground();

const styles = StyleSheet.create({
    root: {
        marginHorizontal: 16,
        marginTop: 10,
        borderBottomColor: '#80CBC4',
        borderBottomWidth: 1,
    },
    value: {
        paddingVertical: 10,
        flexDirection: 'row'
        // todo fix width like display inline clock
    },
    valueText: {
        height: 24,
        textAlignVertical: 'center',
        color: '#f5f5f5'
    },
    valueArrow: {
        width: 24,
        height: 24
    }
});

export default class PayDate extends Component {
    constructor(props) {
        super(props);
        this.state = {
            date: new Date()
        };
    }

    render() {
        let {date} = this.state;
        return (
            <View style={styles.root}>
                <TouchableNativeFeedback background={TouchableBg} onPress={::this.pick}>
                    <View style={styles.value}>
                        <Text style={styles.valueText}>
                            {leftPad(date.getDate(), 2, '0')}
                            .
                            {leftPad(date.getMonth() + 1, 2, '0')}
                            .
                            {date.getFullYear()}
                        </Text>
                        <Image style={styles.valueArrow} source={{uri:'ic_arrow_drop_down_white_24dp'}}/>
                    </View>
                </TouchableNativeFeedback>
            </View>
        );
    }

    async pick() {
        try {
            const {action, year, month, day} = await DatePickerAndroid.open({
                date: this.state.date
            });
            if (action !== DatePickerAndroid.dismissedAction) {
                this.setState({
                    date: new Date(year, month, day)
                });
            }
        } catch ({code, message}) {
            ToastAndroid.show('Ошибка обработки даты', ToastAndroid.SHORT);
            // todo report
        }
    }

    getValue() {
        return this.state.date;
    }
}