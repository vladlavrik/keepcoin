'use strict';
import React, {Component, PropTypes} from 'react';
import {
    StyleSheet,
    TouchableWithoutFeedback,
    Image,
    View
} from 'react-native';

const styles = StyleSheet.create({
    root: {
        padding: 8,
        flex: 0,
        height: 86
    },
    buttonRemove: {
        position: 'absolute',
        top: 0,
        right: 0
    },
    wrapper: {
        paddingVertical: 8,
        paddingHorizontal: 6
    },
    buttonRemoveImage: {
        width: 24,
        height: 24
    },
    image: {
        width: 70,
        height: 70
    }
});

export default class PayPhoto extends Component {
    render() {
        let uri = 'file://' + this.props.path;

        return (
            <View style={styles.root}>
                <Image
                    style={styles.image}
                    source={{uri}}
                    resizeMode="cover"/>

                <TouchableWithoutFeedback onPress={this.props.onRemove}>
                    <View style={styles.buttonRemove}>
                        <Image
                            source={{uri:'ic_cancel_black_24px'}}
                            style={styles.buttonRemoveImage}/>
                    </View>
                </TouchableWithoutFeedback>
            </View>
        );
    }
}

PayPhoto.propTypes = {
    path: PropTypes.string.isRequired,
    onRemove: PropTypes.func.isRequired
};