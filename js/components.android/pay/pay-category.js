'use strict';
import React, {Component, PropTypes} from 'react';
import {
    StyleSheet,
    ScrollView,
    TouchableNativeFeedback,
    Image,
    Text,
    View
} from 'react-native';
import DB from '../../lib/db';

const getListSQl = `
    select _id as id, name
    from categories
    where type=0`;

const TouchableBg = TouchableNativeFeedback.SelectableBackgroundBorderless();

const styles = StyleSheet.create({
    scrollContainer: {
        paddingTop: 26,
        paddingHorizontal: 6
    },
    item: {
        marginHorizontal: 10
    },
    itemButton: {
        borderRadius: 30,
        width: 60,
        height: 60
    },
    itemImage: {
        borderColor: '#B2DFDB',
        borderWidth: 2,
        borderRadius: 30,
        width: 60,
        height: 60
    },
    itemButtonActive: {
        borderRadius: 30,
        backgroundColor: '#B2DFDB'
    },
    itemText: {
        marginTop: 8,
        fontSize: 10,
        color: '#fff',
        textAlign: 'center'
    }
});

export default class PayCategory extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedId: null,
            categories: []
        };
    }
    componentWillMount(){
        this.context.db.query(getListSQl).then(::this.listLoaded, ::this.listLoadFail);
    }

    render() {
        let {selectedId, categories} = this.state;
        return (
            <View>
                <ScrollView
                    contentContainerStyle={styles.scrollContainer}
                    showsHorizontalScrollIndicator={false}
                    horizontal>
                    { categories.map(({id, name}) => (
                       <View key={id} style={styles.item}>
                            <TouchableNativeFeedback background={TouchableBg} onPress={this.select.bind(this, id)}>
                                <View style={[
                                    styles.itemButton,
                                    id === selectedId ? styles.itemButtonActive : null
                                ]}>
                                    <Image
                                        style={styles.itemImage}
                                        source={{
                                            uri: `ic_pay_type_${name}${id === selectedId ? '_active' : ''}_60dp`
                                        }}/>
                                </View>
                            </TouchableNativeFeedback>

                            <Text style={styles.itemText}>{name}</Text>
                        </View>
                    )) }
                </ScrollView>
            </View>
        );
    }

    listLoaded({rows}) {
        this.setState({
            categories: rows.raw()
        });
    }

    listLoadFail(error) {
        // todo show error
    }

    select(selectedId) {
        this.setState({selectedId});
    }

    getValue() {
        return this.state.selectedId;
    }
}

PayCategory.contextTypes = {
    db: PropTypes.instanceOf(DB).isRequired
};