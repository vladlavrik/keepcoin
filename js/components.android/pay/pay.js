'use strict';
import React, {Component, PropTypes} from 'react';
import {
    StyleSheet,
    ToolbarAndroid,
    ToastAndroid,
    ScrollView,
    View
} from 'react-native';
import DB from '../../lib/db';
import PayCategory from './pay-category';
import PayComment from './pay-comment';
import PayTotal from './pay-total';
import PayDate from './pay-date';

const insertTransactionSQl = `
    insert into transactions 
    (total,categoryId,cardId,timestamp,comment,latitude,longitude,address)
    values (?,?,?,?,?,?,?,?)
`;

const insertTransactionPhotoSQl = `
    insert into transactionsPhotos 
    (transactionId,path)
    values (?,?)
`;

const styles = StyleSheet.create({
    toolbar: {
        height: 56,
        backgroundColor: "#009688"
    },
    root: {
        backgroundColor: "#009688",
        paddingBottom: 20,
        flex: 1
    },
    wrapper: {
        backgroundColor: "#009688",
        paddingBottom: 20,
        flex: 1
    }
});

export default class Pay extends Component {

    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        return (
            <View style={styles.root}>
                <ToolbarAndroid
                    titleColor="#fff"
                    title="Добавить покупку"
                    style={styles.toolbar}
                    navIcon={{uri: 'ic_arrow_back_white_24dp'}}
                    onIconClicked={ this.context.navigateBack }
                    actions={[{
                        title: 'Save',
                        icon: {uri: 'ic_done_white_24dp'},
                        show: 'always'
                    }]}
                    onActionSelected={ ::this.save }
                />
                <ScrollView>
                    <View style={styles.wrapper}>

                        <PayCategory ref="category"/>

                        <PayComment ref="comment"/>

                        <PayTotal ref="total"/>

                        <PayDate ref="date"/>
                    </View>
                </ScrollView>
            </View>
        )
    }

    async save() {
        let category = this.refs.category.getValue();
        let {text, photos=[], coordinates=[], address} = this.refs.comment.getValue();
        let {total, card} = this.refs.total.getValue();
        let date = this.refs.date.getValue();

        if (!total) {
            return;
        }


        try {
            let {insertId} = await this.context.db.query(insertTransactionSQl, [
                total,
                category,
                card,
                Math.round(date.getTime() / 1000),
                text,
                coordinates[0],
                coordinates[1],
                address
            ]);

            for(let photo of photos) {
                await this.context.db.query(insertTransactionPhotoSQl, [
                    insertId,
                    photo
                ]);
            }

        } catch (err) {
            console.log(err);
            return;
        }

        ToastAndroid.show('Запись сохранена', ToastAndroid.SHORT);

        this.context.navigateBack()
    }
}

Pay.contextTypes = {
    db: PropTypes.instanceOf(DB).isRequired,
    navigateBack: PropTypes.func.isRequired
};