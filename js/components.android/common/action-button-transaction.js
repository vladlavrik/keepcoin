'use strict';
import React, {Component, PropTypes} from 'react';
import {Image} from 'react-native';
import ActionButton from 'react-native-action-button';

export default class ActionButtonTransaction extends Component {
    render() {
        return (
            // TODO hide on scroll
            <ActionButton
                buttonColor="#078B75">
                <ActionButton.Item
                    buttonColor="#43A047"
                    title="Прибыль"
                    onPress={ ::this.navigateToProfit }>
                    <Image source={{uri: 'ic_add_white_24dp'}} style={{width: 24, height: 24}}/>
                </ActionButton.Item>
                <ActionButton.Item
                    buttonColor="#E53935"
                    title="Оплата"
                    onPress={ ::this.navigateToPay }>
                    <Image source={{uri: 'ic_remove_white_24dp'}} style={{width: 24, height: 24}}/>
                </ActionButton.Item>
            </ActionButton>
        )
    }

    navigateToPay() {
        this.context.navigateTo({name: 'pay'});
    }

    navigateToProfit() {
        this.context.navigateTo({name: 'profit'});
    }
}

ActionButtonTransaction.contextTypes = {
    navigateTo: PropTypes.func.isRequired
};