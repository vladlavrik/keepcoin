'use strict';
import React, {Component, PropTypes} from 'react';
import {
    StyleSheet,
    TouchableNativeFeedback,
    ToastAndroid,
    Image,
    View
} from 'react-native';

import Camera from 'react-native-camera';


const styles = StyleSheet.create({
    preview: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center'
    },
    captureButton: {
        borderRadius: 34,
        alignSelf: 'center',
        width: 68,
        height: 68,
        padding: 10,
        marginBottom: 20
    },
    captureImage: {
        width: 48,
        height: 48
    },
});

const TouchableBg = TouchableNativeFeedback.SelectableBackgroundBorderless();

export default class ImagePickerCameraPhoto extends Component {
    constructor(props) {
        super(props);
        this.state = {
            captured: false
        };
    }

    render() {
        let {captured} = this.state;
        let {quality} = this.props;

        return (
            <View style={{flex: 1}}>
                <Camera
                    ref="camera"
                    style={styles.preview}
                    aspect={Camera.constants.Aspect.fill}
                    captureQuality={Camera.constants.CaptureQuality[quality + 'p']}
                    captureTarget={Camera.constants.CaptureTarget.temp}>
                    <TouchableNativeFeedback
                        background={TouchableBg}
                        onPress={captured ? null : ::this.capture}>
                        <View style={styles.captureButton}>
                            <Image
                                style={styles.captureImage}
                                source={{uri:'ic_camera_white_48dp'}}/>
                        </View>
                    </TouchableNativeFeedback>
                </Camera>

            </View>
        );
    }

    async capture() {
        let {onStartCapture, onCapture} = this.props;
        this.setState({captured: true});

        onStartCapture && onStartCapture();
        try {
            let {path} = await this.refs.camera.capture();
            onCapture(path);
        } catch (err) {
            // TODO report
            ToastAndroid.show('Ошибка снимка', ToastAndroid.SHORT);
        }
    }
}

ImagePickerCameraPhoto.propTypes = {
    quality: PropTypes.oneOf([1080, 720, 480]).isRequired,
    onStartCapture: PropTypes.func.isRequired,
    onCapture: PropTypes.func
};