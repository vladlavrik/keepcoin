
'use strict';
import React, {Component, PropTypes} from 'react';
import {
    StyleSheet,
    TouchableWithoutFeedback,
    ToastAndroid,
    Dimensions,
    ScrollView,
    Image,
    View,
} from 'react-native';
import {
    getImages,
    resizeImage
} from '../../../lib/image-picker-helper';

const styles = StyleSheet.create({
    root: {
        flex: 1,
        backgroundColor:'#00796B'
    },
    wrapper: {
        flexDirection:'row',
        padding: 8,
        flexWrap:'wrap'
    },
    item: {
        margin: 8
    }
});

/* TODO:
 *
 * multi select
 * infinite scroll (auto preload)
 *
 */
const rowItemCount = 4;

export default class ImagePickerCameraRoll extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            resizing: false,
            offset: null,
            list: []
        };

        this.loadImages();
    }

    render() {
        let {width: screenWidth} = Dimensions.get('window');
        let itemSize = (screenWidth - 16 * 2 - 16 * (rowItemCount - 1)) / rowItemCount;
        let {list, loading, resizing} = this.state;

        return (
            <ScrollView style={styles.root}>
                <View style={styles.wrapper}>
                    {list.map(({uri}, key) => (
                        <TouchableWithoutFeedback
                            key={key}
                            onPress={loading || resizing ? null : this.pick.bind(this, uri)}>
                            <Image
                                style={[styles.item, {width: itemSize, height: itemSize}]}
                                source={{uri}}/>
                        </TouchableWithoutFeedback>
                    ))}
                </View>
            </ScrollView>
        );
    }

    async loadImages() {
        try {
            let {edges: list, page_info: pageInfo} = await getImages({
                assetType: this.props.assetType,
                first: 100,
                offset: this.state.offset
            });

            this.setState({
                loading: false,
                list: [...this.state.list, ...list.map(item => item.node.image)],
                offset: pageInfo.has_next_page
                    ? pageInfo.end_cursor
                    : null
            });
        } catch (err) {
            this.close();
            ToastAndroid.show('Ошибка получения фотографий', ToastAndroid.SHORT);
            //TODO report
        }
    }

    async pick(inputUri) {
        this.setState({resizing: true});
        let {width, height} = this.state.list.find(item => item.uri === inputUri);
        let {quality, onPick} = this.props;

        try {
            let uri = await resizeImage(inputUri, width, height, quality);
            onPick(uri);
        } catch (err) {
            this.close();
            ToastAndroid.show('Ошибка обработаки фотографий', ToastAndroid.SHORT);
            //TODO report
        }

    }
}

ImagePickerCameraRoll.defaultProps = {
    assetType: 'All',
};

ImagePickerCameraRoll.propTypes = {
    assetType: PropTypes.string,
    quality: PropTypes.oneOf([1080, 720, 480]).isRequired,
    onStartPick: PropTypes.func,
    onPick: PropTypes.func,
};