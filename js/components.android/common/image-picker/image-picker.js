
'use strict';
import React, {Component, PropTypes} from 'react';
import {
    StyleSheet,
    ToastAndroid,
    Modal
} from 'react-native';
import RNFS from 'react-native-fs';

import ScrollableTabView from 'react-native-scrollable-tab-view';
import CameraPhoto from './camera-photo';
import CameraRoll from './camera-roll';


const styles = StyleSheet.create({
    tabs: {
        backgroundColor: '#00796B',
    },
    underline: {
        backgroundColor: '#ffffff55'
    },
    text: {
    }
});

//TODO fix #ccc border-bottom
//TODO fix image sizes (don't crop)

export default class ImagePicker extends Component {
    constructor(props) {
        super(props);
        this.state = {
            saving: false
        };
    }

    render() {
        let {saving} = this.state;
        let {visible, quality, assetType} = this.props;

        return (
            <Modal
                animationType="slide"
                transparent={true}
                visible={visible}
                onRequestClose={::this.close}>

                <ScrollableTabView
                    locked={saving}
                    prerenderingSiblingsNumber={2}
                    style={styles.tabs}
                    tabBarUnderlineStyle={styles.underline}
                    tabBarTextStyle={styles.text}
                    tabBarActiveTextColor="#ffffff"
                    tabBarInactiveTextColor="#ffffff88">
                    <CameraPhoto
                        tabLabel="Фото"
                        quality={quality}
                        onStartCapture={::this.startSave}
                        onCapture={::this.save}/>
                    <CameraRoll
                        tabLabel="Выбрать"
                        quality={quality}
                        assetType={assetType}
                        onStartPick={::this.startSave}
                        onPick={::this.save}
                    />
                </ScrollableTabView>
            </Modal>
        );
    }

    startSave() {
        this.setState({saving: true});
        this.props.onStartSave();
    }

    async save(uri) {
        let ext = uri.split('.').pop();
        let sourcePath = uri.replace('file://', '');
        let dir = RNFS.DocumentDirectoryPath;

        try {
            let hash = await RNFS.hash(sourcePath, 'md5');
            let path = `${dir}/${hash}.${ext}`;
            await RNFS.moveFile(sourcePath, path);
            this.props.onSaved(path);
        } catch (err) {
            // TODO report
            console.error(err);
            ToastAndroid.show('Ошибка сохраниения фотографии', ToastAndroid.SHORT);
        }
    }

    close() {
        this.props.onCancel();
    }
}

ImagePicker.defaultProps = {
    quality: 1080
};

ImagePicker.propTypes = {
    visible: PropTypes.bool,
    assetType: PropTypes.string,
    quality: PropTypes.oneOf([1080, 720, 480]),
    onStartSave: PropTypes.func,
    onSaved: PropTypes.func,
    onCancel: PropTypes.func.isRequired
};