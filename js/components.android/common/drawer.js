'use strict';
import React, {Component, PropTypes} from 'react';
import {
    StyleSheet,
    View,
    Text,
    Image,
    ScrollView,
    TouchableNativeFeedback
} from 'react-native';

const styles = StyleSheet.create({
    root: {
        flex: 1,
        backgroundColor: '#FAFAFA'
    },
    wrapper: {
        paddingBottom: 16
    },
    content: {
        height: 148,
        marginBottom: 16
    },
    navButton: {
        height: 48,
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: 16
    },
    separator: {
        height: 1,
        marginVertical: 8,
        backgroundColor: '#eee'
    }
});

const TouchableBg = TouchableNativeFeedback.SelectableBackground();

export default class Drawer extends Component {
    render() {
        return (
            <ScrollView
                style={styles.root}>
                <View style={styles.wrapper}>
                    <Image style={styles.content} source={ {uri: 'ic_drawer_bg_300dp' }}/>

                    <TouchableNativeFeedback background={TouchableBg} onPress={this.navigateTo.bind(this, 'transactions')}>
                        <View style={styles.navButton}>
                            <Text>Транзакции</Text>
                        </View>
                    </TouchableNativeFeedback>

                    <TouchableNativeFeedback background={TouchableBg} onPress={this.navigateTo.bind(this, 'summary')}>
                        <View style={styles.navButton}>
                            <Text>Сводка</Text>
                        </View>
                    </TouchableNativeFeedback>

                    <TouchableNativeFeedback background={TouchableBg} onPress={this.navigateTo.bind(this, 'statistics')}>
                        <View style={styles.navButton}>
                            <Text>Статистика</Text>
                        </View>
                    </TouchableNativeFeedback>

                    <TouchableNativeFeedback background={TouchableBg} onPress={this.navigateTo.bind(this, 'places')}>
                        <View style={styles.navButton}>
                            <Text>Места расходов</Text>
                        </View>
                    </TouchableNativeFeedback>

                    <TouchableNativeFeedback background={TouchableBg} onPress={this.navigateTo.bind(this, 'cards')}>
                        <View style={styles.navButton}>
                            <Text>Способы оплаты</Text>
                        </View>
                    </TouchableNativeFeedback>

                    <View style={ styles.separator }/>

                    <TouchableNativeFeedback background={TouchableBg} onPress={this.navigateTo.bind(this, 'settings')}>
                        <View style={styles.navButton}>
                            <Text>Настройки</Text>
                        </View>
                    </TouchableNativeFeedback>

                    <TouchableNativeFeedback background={TouchableBg} onPress={this.navigateTo.bind(this, 'help')}>
                        <View style={styles.navButton}>
                            <Text>Справка/отзыв</Text>
                        </View>
                    </TouchableNativeFeedback>
                </View>
            </ScrollView>
        )
    }

    navigateTo(name) {
        //todo check is not current
        this.context.navigateTo({name});
    }

}

Drawer.contextTypes = {
    navigateTo: PropTypes.func.isRequired
};