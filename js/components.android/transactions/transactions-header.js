'use strict';
import React, {Component, PropTypes} from 'react';
import {
    StyleSheet,
    Text
} from 'react-native';

import leftPad from '../../lib/utils/left-pad';

const styles = StyleSheet.create({
    root: {
        height: 46,
        paddingBottom: 2,
        paddingLeft: 72,
        textAlignVertical: 'center',
        color: '#BDBDBD',
        borderTopWidth: 1,
        borderTopColor: '#EEEEEE',
    }
});

export default class TransactionsHeader extends Component {
    render() {
        return (
            <Text style={styles.root}>
                {leftPad(this.props.date.getDate(), 2, '0')}
                .
                {leftPad(this.props.date.getMonth() + 1, 2, '0')}
                .
                {this.props.date.getFullYear()}
            </Text>
        )
    }
}

TransactionsHeader.propsTypes = {
    date: PropTypes.instanceOf(Date).isRequired,
};
