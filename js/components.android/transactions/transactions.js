'use strict';
import React, {Component, PropTypes} from 'react';
import {
    StyleSheet,
    ToolbarAndroid,
    ListView,
    View,
} from 'react-native';
import DB from '../../lib/db';
import ActionButtonTransaction from '../common/action-button-transaction';
import TransactionsItem from './transactions-item';
import TransactionsHeader from './transactions-header';

const getListSQl = `
    select
        transactions._id as id,
        transactions.total,
        cards.name as card,
        categories.name as category,
        (CASE categories.type WHEN 0 THEN 'pay' WHEN 1 THEN 'profit' END)  as categoryType,
        (CASE WHEN transactions.latitude || transactions.longitude THEN 1 ELSE 0 END) as hasLocation,
        transactions.timestamp,
        transactions.comment,
        count(transactionsPhotos.path) as photosCount
    from transactions
    left outer join cards on transactions.cardId = cards._id
    left outer join categories on transactions.categoryId = categories._id
    left outer join transactionsPhotos on transactionsPhotos.transactionId = transactions._id
    group by id
    order by timestamp desc
`;



const styles = StyleSheet.create({
    toolbar: {
        height: 56,
        backgroundColor: "#009688"
    },
    root: {
        flex: 1,
        backgroundColor: "#fff"
    }
});


export default class Transactions extends Component {
    constructor() {
        super();

        let ds = new ListView.DataSource({
            rowHasChanged: (r1, r2) => r1 !== r2,
            sectionHeaderHasChanged: (s1, s2) => s1 !== s2,
        });

        this.state = {
            dataSource: ds,
        };

    }

    componentWillMount(){
        this.context.db.query(getListSQl).then(::this.listLoaded, ::this.listLoadFail);
    }

    render() {
        return (
            <View style={styles.root}>

                <ToolbarAndroid
                    titleColor="#fff"
                    title="Трансакции"
                    style={styles.toolbar}
                    navIcon={{uri: 'ic_menu_white_24dp'}}
                    onIconClicked={ this.context.openDrawer }
                />

                <ListView
                    style={{}}
                    dataSource={this.state.dataSource}
                    renderSectionHeader={(list, sectionId)=> (
                      <TransactionsHeader date={new Date(...sectionId.split('.').reverse())}/>
                    )}
                    renderRow={(transaction, sectionID, rowID) => (
                        <TransactionsItem
                            key={transaction.id}
                            transaction={transaction}
                            onPress={this.navigateToTransaction.bind(this, sectionID, rowID)} />
                    )}
                />

                <ActionButtonTransaction/>

            </View>
        )
    }

    listLoaded({rows}) {
        let dataSource = {};
        for(let i=0; i < rows.length; i++) {
            let dataItem = rows.item(i);
            let date = new Date(dataItem.timestamp * 1000);
            let groupId = [
                date.getDate(),
                date.getMonth(),
                date.getFullYear()
            ].join('.');

            if (dataSource.hasOwnProperty(groupId)) {
                dataSource[groupId].push(dataItem);
            } else {
                dataSource[groupId] = [dataItem];
            }
        }

        this.setState({
            dataSource: this.state.dataSource.cloneWithRowsAndSections(dataSource)
        })
    }

    listLoadFail(error) {
        // todo show error
    }

    navigateToTransaction(sectionID, rowID) {
        let {dataSource} = this.state;
        // TODO FIX it!
        let sectionIndex = dataSource.sectionIdentities.indexOf(sectionID);
        let data = this.state.dataSource.getRowData(sectionIndex, rowID);

        this.context.navigateTo({
            name: 'transaction',
            transaction: {
                id: data.id,
                total: data.total,
                comment: data.comment,
                category: data.category,
                categoryType: data.categoryType,
                card: data.card,
                timestamp: data.timestamp,
                hasLocation: !!data.hasLocation,
                hasPhotosCount: !!data.photosCount
            }
        });
    }
}

Transactions.contextTypes = {
    db: PropTypes.instanceOf(DB).isRequired,
    openDrawer: PropTypes.func.isRequired,
    navigateTo: PropTypes.func.isRequired
};

