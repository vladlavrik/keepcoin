'use strict';
import React, {Component, PropTypes} from 'react';
import {
    StyleSheet,
    Image,
    Text,
    View,
    TouchableNativeFeedback
} from 'react-native';


const TouchableBg = TouchableNativeFeedback.SelectableBackground();

const styles = StyleSheet.create({
    root: {
        height: 72,
        backgroundColor: '#fff',
        flexDirection: 'row',
        alignItems: 'center'
    },
    iconBox: {
        marginHorizontal: 16,
        borderRadius: 20,
        backgroundColor: '#E0F2F1'
    },
    icon: {
        width: 40,
        height: 40,
    },
    contentBox: {
        flex: 1
    },
    sideBox: {
        marginHorizontal: 16,
        justifyContent: 'flex-end'
    },
    category: {
        fontSize: 16,
        color: '#9E9E9E',
        textAlignVertical: 'center',
        height: 22
    },
    description: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    flagImage: {
        height: 16,
        marginRight: 6,
        opacity: 0.2
    },
    flagLocationImage: {
        width: 12
    },
    flagPhotoImage: {
        width: 18
    },
    comment: {
        fontSize: 14,
        color: '#BDBDBD',
        height: 22,

    },
    total: {
        alignSelf: 'flex-end',
        textAlign: 'right',
        fontWeight: '500'
    },
    totalPay: {
        color: '#E53935'
    },
    totalProfit: {
        color: '#43A047'
    },
    card: {
        textAlign: 'right',
        color: '#BDBDBD'
    }
});


export default class TransactionsItem extends Component {
    render() {
        let {category, categoryType, comment, total, card, hasLocation, photosCount} = this.props.transaction;
        return (
            <TouchableNativeFeedback
                background={TouchableBg}
                onPress={this.props.onPress}>
                <View style={styles.root}>
                    <View style={styles.iconBox}>
                        <Image
                            style={styles.icon}
                            source={{uri: `ic_${categoryType}_type_${category}_40dp`}}/>
                    </View>

                    <View style={styles.contentBox}>
                        <Text style={styles.category}>
                            {category}
                        </Text>
                        <View style={styles.description}>
                            {!!hasLocation && (
                                <Image
                                    style={[styles.flagImage, styles.flagLocationImage]}
                                    source={{uri:'ic_place_black_16dp'}}/>
                            )}
                            {!!photosCount && (
                                <Image
                                    style={[styles.flagImage, styles.flagPhotoImage]}
                                    source={{uri:'ic_camera_black_16dp'}}/>
                            )}
                            <Text style={styles.comment} numberOfLines={1}>
                                {comment
                                    ? comment.replace(/\s+|\n/g, ' ')
                                    : '(нет описания)'}
                            </Text>
                        </View>
                    </View>
                    <View style={styles.sideBox}>
                        <Text style={[styles.total, categoryType === 'profit' ? styles.totalProfit : styles.totalPay]}>
                            {total}₴
                        </Text>
                        <Text style={styles.card}>
                            {card}
                        </Text>
                    </View>
                </View>
            </TouchableNativeFeedback>
        )
    }
}

TransactionsItem.propsTypes = {
    transaction: PropTypes.shape({
        id: PropTypes.number.isRequired,
        type: PropTypes.oneOf(['pay', 'profit']).isRequired,
        category: PropTypes.string.isRequired,
        comment: PropTypes.string,
        total: PropTypes.number.isRequired,
        card: PropTypes.string.isRequired,
    }).isRequired,
    onPress: PropTypes.func.isRequired,
};
