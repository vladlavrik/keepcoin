'use strict';

export default function (str, count, char=' ') {
    return (char.repeat(count) + str).slice(-count)
}