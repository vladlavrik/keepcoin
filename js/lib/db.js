'use strict';
import {ToastAndroid} from 'react-native';
import SQLite from 'react-native-sqlite-storage';

export default class DB {
    constructor({name, createFromLocation}) {
        this._options = {
            name,
            createFromLocation
        };
        this._openedDB = false;
        this._openingProcess = false;

        this.open();
    }

    /**
     * @public
     * @returns {Promise}
     */
    open() {
        if(this._openedDB) {
            return Promise.resolve();
        }

        if(this._openingProcess) {
            return this._openingProcess;
        }

        let promise = new Promise((resolve, reject) => {
            SQLite.openDatabase(this._options, db => {
                this._openSuccess(db);
                resolve(db);
            }, error => {
                this._openError();
                reject(error)
            });
        });

        this._openingProcess = promise;

        return promise;
    }


    /**
     * @public
     * @returns {Promise}
     */
    query(sql, arg=[]) {

        if(this._openedDB) {
            return this._executeQuery(sql, arg)
        }

        if(this._openingProcess) {
            return this._openingProcess
                .then(() => this._executeQuery(sql, arg));
        }

        return this.open()
            .then(() => this._executeQuery(sql, arg));
    }


    /**
     * @private
     */
    _openSuccess(db) {
        this._openedDB = db;
        this._openingProcess = null;
    }

    /**
     * @private
     */
    _openError() {
        // TODO report
        ToastAndroid.show('Ошибка работы с базой данных', ToastAndroid.SHORT);
        this._openingProcess = null;
    }

    /**
     * @private
     */
    _executeQuery(sql, arg) {
        return new Promise((resolve, reject) => {
            this._openedDB.executeSql(sql, arg, result => {
                this._executeQuerySuccess(result);
                resolve(result);
            }, error => {
                this._executeQueryError(error);
                reject(error);
            });

        });
    }

    /**
     * @private
     */
    _executeQuerySuccess() {

    }

    //noinspection JSMethodCanBeStatic
    /**
     * @private
     */
    _executeQueryError() {
        // TODO report
        ToastAndroid.show('Ошибка работы с базой данных', ToastAndroid.SHORT);
    }
}
