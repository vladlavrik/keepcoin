'use strict';
import {
    CameraRoll,
    ImageStore,
    ImageEditor,
} from 'react-native';
import RNFS from 'react-native-fs';


export function getImages({assetType='All', first=100, after}) {
    return CameraRoll.getPhotos({
        assetType,
        first,
        after
    });
}

/**
 * @param {string} uri
 * @param {number} width
 * @param {number} height
 * @param {number} quality - one of: 1080|720|480
 */
export function resizeImage(uri, width, height, quality) {
    let isHorizontal = width >= height;
    //todo check case if width < quality
    let ratio = quality / (isHorizontal ? height : width);
    width = width * ratio;
    height = height * ratio;

    let cropData = {
        offset: {x: 0, y: 0},
        size: {width, height}
    };

    return new Promise((resolve, reject) => {
        ImageEditor.cropImage(uri, cropData, resolve, reject);
    });
}