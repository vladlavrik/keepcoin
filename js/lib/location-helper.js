'use strict';
import {
    PermissionsAndroid,
} from 'react-native';

import config from '../config.json';

const geoCoordinatesOption = {
    enableHighAccuracy: false,
    timeout: 30000,
    maximumAge: 10000
};
/* TODO
 * get language form gapi from settings
 * user select variant address
 */

export async function getCoordinates() {
    return new Promise((resolve, reject) => {
        navigator.geolocation.getCurrentPosition(
            result => resolve(result.coords),
            error => reject(error),
            geoCoordinatesOption
        );
    });
}


export async function getAddress(latitude, longitude) {
    let url = `https://maps.googleapis.com/maps/api/geocode/json?latlng=${latitude},${longitude}&key=${config.googleMapApiKey}&language=uk`;
    let result = await fetch(url, {
        method: 'GET',
        headers: {
            'Accept': 'application/json'
        }
    });

    if (!result.ok) {
        throw new Error('Error get address through Google maps Api: Network error');
    }

    let resultJSON = await result.json();

    if (resultJSON.status !== 'OK') {
        throw new Error('Error get address through Google maps Api: Google Server error');
    }

    return resultJSON.results[0].formatted_address;
}
